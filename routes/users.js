var express = require('express');
var router = express.Router();
var pacienteC = require('../controller/PacienteControl');
var paciente = new pacienteC();
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.get('/buscar_paciente',  paciente.buscar);
module.exports = router;
