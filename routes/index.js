var express = require('express');
var router = express.Router();
var passport = require('passport');
var persona = require('../controller/PersonaControl');
var personaC = new persona();
var cuenta = require('../controller/CuentaControl');
var cuentaC = new cuenta();
var pacienteC = require('../controller/PacienteControl');
var paciente = new pacienteC();
var fotoC = require('../controller/FotoControl');
var foto = new fotoC();

var auth = function (req, res, next) {
    if(req.isAuthenticated()) {
        next();
    } else {
        req.flash('error', "Debes iniciar sesion !");
        res.redirect("/");
    }
}

router.get('/', function (req, res, next) {
    //console.log(req.user);
    if (req.isAuthenticated()) {
        res.render('index', {title: 'Principal', andrea: "fragmentos/principal", sesion: true, usuario: req.user.nombre});
    } else {
        res.render('index', {title: 'Medicos', msg: {error: req.flash('error'), info: req.flash('info')}});
    }

});
//para registro
router.get('/registro', function (req, res, next) {
    res.render('index', {title: 'Registrate', sesion: true, andrea: "fragmentos/registro_medico"});
});
router.post('/registro', personaC.guardar);

router.post('/inicio_sesion',
        passport.authenticate('local-signin',
                {successRedirect: '/',
                    failureRedirect: '/',
                    failureFlash: true}
        ));

router.get('/cerrar_sesion', auth, cuentaC.cerrar_yanela);
//paciente
router.get('/administracion/pacientes', auth, paciente.visualizar);
router.post('/administracion/pacientes/guardar', auth, paciente.guardar);
router.get('/administracion/pacientes/modificar/:external', auth, paciente.visualizar_modificar);
router.post('/administracion/pacientes/update', auth, paciente.modificar);

router.get('/administracion/perfil', auth, foto.visualizar);
router.post('/administracion/perfil/foto', auth, foto.subir_foto);

module.exports = router;
