'use strict';
var models = require('../models/');
var persona = models.persona;
var formidable = require('formidable');
var extensiones = ["jpg", "png", "gif"];
var maxSize = 1 * 1024 * 1024;
var fs = require('fs');
class FotoControl {
    visualizar(req, res) {
        persona.findOne({where: {external_id:req.user.id}}).then(function (yanela) {
            if(yanela) {
                res.render('index', {title: "Principal",
                    andrea: 'fragmentos/perfil/perfil',
                    sesion: true,
                    persona: yanela,
                    msg: {error: req.flash('error'),ok: req.flash('success')}
                });
            } else {
                req.flash('error', "No se encontro nada");
                res.redirect("/");
            }
        });
    }
    
    subir_foto(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            if (files.archivo.size <= maxSize) {
                var extension = files.archivo.name.split(".").pop().toLowerCase();
                if (extensiones.includes(extension)) {
                    var external = fields.external;
                    var nombreFoto = external + "." + extension;
                    fs.rename(files.archivo.path, "public/uploads/" + nombreFoto, 
                    function (err) {
                        if(err) {
                            req.flash('error', "Hubo un error "+err);
                            res.redirect("/administracion/perfil");
                        } else {
                            persona.findOne({where: {external_id: external}})
                                    .then(function (result) {
                                result.foto = nombreFoto;
                                result.save().then(function (ok) {
                                    fs.exists(files.archivo.path, function (exists) {
                                        if(exists) 
                                            fs.unlinkSync(files.archivo.path);
                                    });
                                    req.flash('info', "Se ha cambiado su foto de perfil");
                                    res.redirect("/administracion/perfil");
                                });
                                
                            });
                        }
                    });
                } else {
                    req.flash('error', "El tipo de archivo no es valido, debe ser png, jpg, o gif");
                    res.redirect("/administracion/perfil");
                }
            } else {
                req.flash('error', "El tamano no puede superar 1 MB");
                res.redirect("/administracion/perfil");
            }
        });
    }
}
module.exports = FotoControl;







