'use strict';
var models = require('../models');
var uuid = require('uuid');
var sequelize = require('sequelize');
/**
 * Clase que permite manipular los datos del modelo con la vista
 */
class PacienteControl {
    /**
     * Funcion que permite mostrar la vista del listado de pacientes
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina ........
     */
    visualizar(req, res) {
        var pacienteC = models.paciente;
        pacienteC.findAll({include: [{model: models.historia, as: 'historia'}]})
                .then(function (lista) {
            var nro_historia = 'HIS-' + (lista.length + 1);
            res.render('index',
                    {title: 'Pacientes',
                        andrea: "fragmentos/paciente/lista",
                        sesion: true,
                        nro: nro_historia,
                        listado: lista,
                        msg: {error: req.flash('error'), info: req.flash('info')}
                    });
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/');
        });

    }
    /**
     * Funciuon que permite guardar los datos del Paciente con su Historia
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina ........
     */
    guardar(req, res) {
        var dataP = {
            cedula: req.body.cedula,
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            direccion: req.body.direccion,
            external_id: uuid.v4(),
            historia: {
                nro_historia: req.body.nro_his,
                enfermedades: req.body.enf,
                enfer_hede: req.body.enf_her,
                habitos: req.body.hab,
                contacto: req.body.contacto,
                external_id: uuid.v4()
            }
        };
        var paciente = models.paciente;
        paciente.create(dataP, {include: [{model: models.historia, as: 'historia'}]})
                .then(function (result) {
            req.flash('info', 'Se ha registrado correctamente');
            res.redirect('/administracion/pacientes');
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/administracion/pacientes');
        });
    }

    visualizar_modificar(req, res) {
        var external = req.params.external;
        var pacienteC = models.paciente;
        pacienteC.findAll({where: {external_id: external}, include: [{model: models.historia, as: 'historia'}]}).then(function (yanela) {
            if (yanela.length > 0) {
                var rojas = yanela[0];
                res.render('index',
                        {title: 'Pacientes',
                            andrea: "fragmentos/paciente/modificar",
                            sesion: true,
                            paci: rojas,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
            } else {
                req.flash('error', 'No existe el dato a buscar');
                res.redirect('/administracion/pacientes');
            }
        }).error(function (error) {
            req.flash('error', 'se produjo un error');
            res.redirect('/administracion/pacientes');
        });

    }

    modificar(req, res) {
        var pacienteC = models.paciente;
        pacienteC.findAll({where: {external_id: req.body.token},
            include: [{model: models.historia, as: 'historia'}]}).then(function (yanela) {
            if (yanela.length > 0) {
                var rojas = yanela[0];
                rojas.apellidos = req.body.apellidos;
                rojas.nombres = req.body.nombres;
                rojas.fecha_nac = req.body.fecha_nac;
                rojas.edad = req.body.edad;
                rojas.direccion = req.body.direccion;
                rojas.save().then(function (result) {
                    var historial = rojas.historia;
                    historial.nro_historia = req.body.nro_his;
                    historial.enfermedades = req.body.enf;
                    historial.enfer_hede = req.body.enf_her;
                    historial.habitos = req.body.hab;
                    historial.contacto = req.body.contacto;
                    historial.save();
                    req.flash('info', 'Se ha modificado correctamente');
                    res.redirect('/administracion/pacientes');
                }).error(function (error) {
                    console.log(error);
                    req.flash('error', 'No se pudo modificar');
                    res.redirect('/administracion/pacientes');
                });

            } else {
                req.flash('error', 'No existe el dato a buscar');
                res.redirect('/administracion/pacientes');
            }
        }).error(function (error) {
            req.flash('error', 'se produjo un error');
            res.redirect('/administracion/pacientes');
        });
    }

    buscar(req, res) {
        const Op = sequelize.Op;
        var criterio = req.query.criterio;
        var texto = req.query.texto;
        var data = {};
        var pacienteC = models.paciente;
        if (criterio === 'todos') {
            pacienteC.findAll({include: [{model: models.historia, as: 'historia'}]}).then(function (lista) {

                lista.forEach(function (item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro: item.historia.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function (error) { });
        } else if (criterio === "cedula") {
            pacienteC.findAll({where: {cedula: texto}, include: [{model: models.historia, as: 'historia'}]}).then(function (lista) {

                lista.forEach(function (item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro: item.historia.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function (error) { });
        } else if (criterio === "historial") {
            pacienteC.findAll({include: [{model: models.historia, as: 'historia', where: {'nro_historia': texto}}]}).then(function (lista) {
                lista.forEach(function (item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro: item.historia.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function (error) { });
        } else if (criterio === "apellidos") {
            console.log(Op.like);
            pacienteC.findAll({where: {apellidos: {[Op.like]: '%' + texto+'%'}}, include: [{model: models.historia, as: 'historia'}]}).then(function (lista) {

                lista.forEach(function (item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro: item.historia.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function (error) { });
        }

        //console.log(req.query.criterio);
        //res.json(req.query.criterio);
    }
}
module.exports = PacienteControl;