'use strict';
class CuentaControl {
    cerrar_yanela (req, res) {
        req.session.destroy();
        res.redirect('/');
    }
}
module.exports = CuentaControl;
