'use strict';
module.exports = (sequelize, DataTypes) => {
  const Historia = sequelize.define('historia', {
    nro_historia: DataTypes.STRING(10),
    enfermedades: DataTypes.STRING,
    enfer_hede: DataTypes.STRING,
    habitos: DataTypes.STRING,    
    external_id: DataTypes.UUID,
    contacto: DataTypes.STRING
  }, {freezeTableName: true});
  Historia.associate = function(models) {
    // associations can be defined here
    Historia.belongsTo(models.paciente, {foreignKey: 'id_paciente'});
  };
  return Historia;
};