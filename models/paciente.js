'use strict';
module.exports = (sequelize, DataTypes) => {
  const paciente = sequelize.define('paciente', {
    cedula: DataTypes.STRING(10),
    apellidos: DataTypes.STRING(40),
    nombres: DataTypes.STRING(40),
    edad: DataTypes.INTEGER,
    fecha_nac: DataTypes.DATEONLY,
    external_id: DataTypes.UUID,
    direccion: DataTypes.STRING
  }, {freezeTableName: true});
    paciente.associate = function(models) {
      paciente.hasOne(models.historia, {foreignKey: 'id_paciente', as: 'historia'});
    };
  return paciente;
};